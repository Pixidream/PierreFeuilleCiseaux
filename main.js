// récupère les éléments à update
const yourImgToUpdate = $('#yourChoiceImg');
const yourTextToUpdate = $('#yourChoiceText');
const myImgToUpdate = $('#myChoiceImg');
const myTextToUpdate = $('#myChoiceText');
const alertResult = $('#alertResult');
const alertModal = $('#resultModal');
const alertYourScore = $('#yourScore');
const alertMyScore = $('#myScore');
const recordYourScore = $('#yourScore span');
const recordMyScore = $('#myScore span');
const startOver = $('#startOver');
const letsPlay = $('#letsPlay');

// On store les images dans un objet
let images = {
	Pierre: "./img/Rock.png",
	Papier: "./img/Paper.png",
	Ciseaux: "./img/Scissors.png",
	question: "./img/question.png"
};

// On store les 3 alertes dans un objets, loose win et égalité
let alertDiv = {
	loose: '<div class="alert alert-danger" role="alert"><strong>Perdu !</strong> On dirait que je suis le plus fort haha 😘 !</div>',
	win: '<div class="alert alert-success" role="alert"><strong>Tu gagnes ... </strong> Rejoues! Je dois prendre ma revanche 😠 !</div>',
	draw: '<div class="alert alert-info" role="alert"><strong>Égalitée !</strong> c\'est une égalité, Rejoue jeune Padawan 😌 !</div>'
};

// variables de score
let yourScore = 0;
let myScore = 0;

// Cette fonction sera appelé pour mettre à jour les images
const updateImg = (img, val) => {
	switch(val){
		case "Pierre":
			img.attr('src', images.Pierre);
			break;
		case "Papier":
			img.attr('src', images.Papier);
			break;
		case "Ciseaux":
			img.attr('src', images.Ciseaux);
			break;
		default:
			img.attr('src', images.question);
	}
};

// Cette fonction reset les images au choix
const resetImg = () => {
	myImgToUpdate.attr("src", images.question);
	myTextToUpdate.text("");
	yourImgToUpdate.attr("src", images.question);
	yourTextToUpdate.text("");
};

// Cette fonction met à jour le contenu des alertes
const updateAlert = (alert) => {
	alertResult.append(alert);
	alertModal.modal('toggle');
};

// Cette fonction met à jour le score
const updateScore = (score) => {
	(score === "yourScore") ? yourScore += 1 : myScore += 1;
};

// Appel les fonctions de mise a jour d'alerte et de score en meme temps
const alertScore = (theAlert, theScore) => {
	updateAlert(theAlert);
	updateScore(theScore);
};

// Désactive le bouton jouer tant qu'un choix n'a pas été fait
const disablePlay = () => {
	(yourTextToUpdate.text() === "") ? letsPlay.attr('disabled', true) : letsPlay.attr('disabled', false);
};

const resetPlayBtn = () => {
	$('#alertResult').children().remove();
	$('#letsPlay').text("Jouer");
};

// Mise en place du jeu
$(document).ready( function() {

	// Désactive le bouton jouer pendant le chargement de la page.
	$('#letsPlay').attr('disabled', true);

	// Remet à 0 les choix et images et rejoue
	// Bouton qui remet tous les états à 0.
	$('#closeReset').click( function() {
		resetImg();
		resetPlayBtn();
		disablePlay();
	});

	// récupère le choix utilisateur et joue
	// Bouton jouer.
	$('.btn-choice').click( function() {

		var value = $(this).text();
		yourTextToUpdate.text(value);
		updateImg(yourImgToUpdate, value);
		disablePlay();

	});

	// écupère le choix de l'ordinateur et joue
	// joue au jeu.
	$('#letsPlay').click( function() {

		if ($(this).text() === "Remettre à 0") {

			resetImg();
			resetPlayBtn();
			disablePlay();

		} else {

			var userChoice = $('#yourChoiceText').text();
			var computerChoice = Math.random();

			if (computerChoice < 0.34) {
				computerChoice = "Pierre";
			} else if (computerChoice <= 0.67) {
				computerChoice = "Papier";
			} else {
				computerChoice = "Ciseaux";
			};

			myTextToUpdate.text(computerChoice);
			updateImg(myImgToUpdate, computerChoice);

			if (userChoice === computerChoice) {
				updateAlert(alertDiv.draw);
			} else if (userChoice === "Pierre") {
				if (computerChoice === "Ciseaux") {
					alertScore(alertDiv.win, "yourScore");
				} else {
					alertScore(alertdiv.loose, "myScore");
				}
			} else if (userChoice === "Papier") {
				if (computerChoice === "Pierre") {
					alertScore(alertDiv.win, "yourScore");
				} else {
					alertScore(alertDiv.loose, "myScore");
				}
			} else if (userChoice === "Ciseaux") {
				if (computerChoice === "Pierre") {
					alertScore(alertDiv.loose, "myScore");
				} else {
					alertScore(alertDiv.win, "yourScore");
				}
			}

			$(this).text("Remettre à 0");

			recordMyScore.text(myScore);
			recordYourScore.text(yourScore);

			if (myScore > yourScore) {
				alertMyScore.removeClass('alert-info alert-danger').addClass('alert-success');
				alertYourScore.removeClass('alert-info alert-success').addClass('alert-danger');
			} else if (myScore < yourScore) {
				alertMyScore.removeClass('alert-info alert-success').addClass('alert-danger');
				alertYourScore.removeClass('alert-info alert-danger').addClass('alert-success');
			} else {
				alertMyScore.removeClass('alert-success alert-danger').addClass('alert-info');
				alertYourScore.removeClass('alert-danger alert-success').addClass('alert-info');
			}

		};

	});

	$('#startOver').click( function() {

		resetImg();
		resetPlayBtn();

		yourScore = 0;
		myScore = 0;

		recordMyScore.text(myScore);
		recordYourScore.text(yourScore);

		alertMyScore.removeClass('alert-success alert-danger');
		alertYourScore.removeClass('alert-success alert-danger');
		alertMyScore.addClass('alert-info');
		alertYourScore.addClass('alert-info');

	});

});
